import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import enums.IBANS;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import payload.IBAN;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BankAccount {
  private final String AUTHORIZATION_HEADER_NAME = "X-Auth-Key";
  private Response res = null; //Response object
  private JsonPath jp = null; //JsonPath object
  private IBAN iban = new IBAN();
  private Assertion assertion = new Assertion();

  @Before
  public void setup() {
    //Test Setup
    RestUtil.setBaseURI("https://dev.horizonafs.com/ecommercerest/"); //Setup Base URI
    RestUtil.setBasePath("api/v3/validate/bank-account"); //Setup Base Path
    RestUtil.setContentTypeWithCharset(ContentType.JSON, "UTF-8");
    RestUtil.setHeader("Accept", ContentType.JSON.getAcceptHeader());
    RestUtil.setHeader(AUTHORIZATION_HEADER_NAME, "Q7DaxRnFls6IpwSW1SQ2FaTFOf7UdReAFNoKY68L");
  }

  @Test
  public void validIBANtest() {
    res = RestUtil.getPostResponse(iban.getJsonBody(IBANS.validIBAN.getIBAN()));

    assertion.checkStatusCode(res, 200);
  }

  @Test
  public void invalidIBANtest() {
    res = RestUtil.getPostResponse(iban.getJsonBody(IBANS.invalidIBAN.getIBAN()));

    assertion.checkRiskCheckMessageCode(res, "200.908");
  }

  @Test
  public void unsupportedCountryTest() {
    res = RestUtil.getPostResponse(iban.getJsonBody(IBANS.unsupportedCountryIBAN.getIBAN()));

    assertion.checkRiskCheckMessageCode(res, "200.909");
  }

  @Test
  public void shortIBANlengthTest() {
    String ibanCode = IBANS.shortIBAN.getIBAN();
    res = RestUtil.getPostResponse(iban.getJsonBody(ibanCode));

    assertion.checkIBANlength(res, ibanCode.length());
  }

  @Test
  public void longIBANlengthTest() {
    String ibanCode = iban.getJsonBody(IBANS.longIBAN.getIBAN());
    res = RestUtil.getPostResponse(ibanCode);

    assertion.checkIBANlength(res, ibanCode.length());
  }

  @Test
  public void authorizationFailsTest() {
    RestUtil.setHeader(AUTHORIZATION_HEADER_NAME, "invalid_uthorization");
    res = RestUtil.getPostResponse(iban.getJsonBody(IBANS.validIBAN.getIBAN()));

    assertion.checkStatusCode(res, 401);
  }

  @After
  public void tearDown() {
    //Reset Values
    RestUtil.resetBaseURI();
    RestUtil.resetBasePath();
  }
}
