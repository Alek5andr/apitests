import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;

public class RestUtil {
  private static Map<String, String> headersMap = new HashMap<String, String>();
  private static RequestSpecification requestSpecification = null;
  
  /*
  ***Sets Base URI***
  Before starting the test, we should set the RestAssured.baseURI
  */
  public static void setBaseURI (String baseURI){
    RestAssured.baseURI = baseURI;
  }

  /*
  ***Sets base path***
  Before starting the test, we should set the RestAssured.basePath
  */
  public static void setBasePath(String basePath){
    RestAssured.basePath = basePath;
  }

  /*
  ***Reset Base URI (after test)***
  After the test, we should reset the RestAssured.baseURI
  */
  public static void resetBaseURI (){
    RestAssured.baseURI = null;
  }

  /*
  ***Reset base path (after test)***
  After the test, we should reset the RestAssured.basePath
  */
  public static void resetBasePath() {
    RestAssured.basePath = null;
  }

  /*
  ***Sets ContentType***
  We should set content type as JSON or XML before starting the test
  */
  public static void setContentType (ContentType type){
    given().contentType(type);
  }

  public static void setContentTypeWithCharset (ContentType type, String charset){
    requestSpecification = given().contentType(type.withCharset(charset));
  }

  public static void setHeader(String key, String value) {
    headersMap.put(key, value);
  }

  public static Response getPostResponse(String jsonBody) {
    return requestSpecification.headers(headersMap).body(jsonBody).post();
  }
}
