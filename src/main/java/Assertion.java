import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import org.junit.Assert;

public class Assertion {
  private final String ERROR_400xx = "400.00";

  public void checkStatusCode(Response res, int expectedStatusCode) {
    Assert.assertEquals("Received status code - " + res.getStatusCode() + getResponseBody(res),
        expectedStatusCode,
        res.getStatusCode());
  }

  public void checkRiskCheckMessageCode(Response res, String expectedCode) {
    String receivedCode = getRiskCheckMessageCode(res);
    Assert.assertEquals("Expected code - " + expectedCode + "; Received code - " + receivedCode + getResponseBody(res),
        expectedCode,
        receivedCode);
  }

  public void checkIBANlength(Response res, int lengthNumber) {
    checkStatusCode(res, 400);
    if (lengthNumber < 7) {
      Assert.assertEquals("IBAN code's length is >= 7: " + lengthNumber, ERROR_400xx + "6", getBodyErrorCode(res));
    } else if(lengthNumber > 34) {
      Assert.assertEquals("IBAN code's length is <= 34: " + lengthNumber, ERROR_400xx + "5", getBodyErrorCode(res));
    }
  }

  private String getResponseBody(Response res) {
    return "\n" + "Body response: " + res.body().asString();
  }

  private int getStatusCode(Response res) {
    return res.getStatusCode();
  }

  private String getRiskCheckMessageCode(Response res) {
    return getJsonPath(res).getString("riskCheckMessages.code[0]");
  }

  private String getBodyErrorCode(Response res) {
    return getJsonPath(res).getString("code[0]");
  }

  private String getBodyErrorMessage(Response res) {
    return getJsonPath(res).getString("message[0]");
  }

  /*
   ***Returns JsonPath object***
   * First convert the API's response to String type with "asString()" method.
   * Then, send this String formatted json response to the JsonPath class and return the JsonPath
   */
  private JsonPath getJsonPath (Response res) {
    return new JsonPath(res.asString());
  }
}
