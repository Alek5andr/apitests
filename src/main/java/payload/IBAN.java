package payload;

import org.json.simple.JSONObject;

public class IBAN {
  private JSONObject jsonBody = new JSONObject();

  public String getJsonBody(String iban) {
    jsonBody.put("bankAccount", iban);
    return jsonBody.toJSONString();
  }
}
